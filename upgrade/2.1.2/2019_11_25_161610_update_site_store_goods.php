<?php

namespace We7\V212;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1574669770
* @version 2.1.2
*/

class UpdateSiteStoreGoods {

	/**
	 *  执行更新
	 */
	public function up() {
		$columus = array(
			'aliapp_num', 'baiduapp_num', 'phoneapp_num', 'toutiaoapp_num', 'webapp_num', 'xzapp_num'
		);
		foreach ($columus as $columu) {
			if (!pdo_fieldexists('site_store_goods', $columu)) {
				$sql = "ALTER TABLE " . tablename('site_store_goods') . " ADD COLUMN `{$columu}` INT(10) NOT NULL DEFAULT 0";
				pdo_run($sql);
			}
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
