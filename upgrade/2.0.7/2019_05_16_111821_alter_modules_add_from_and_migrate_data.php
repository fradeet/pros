<?php

namespace We7\V207;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1557976701
 * @version 2.0.7
 */

class AlterModulesAddFromAndMigrateData {

	/**
	 *  执行更新
	 */
	public function up() {
		load()->model('cloud');
		load()->model('module');
		if (!pdo_fieldexists('modules','from')) {
			pdo_query("ALTER TABLE " . tablename('modules') . " ADD `from` varchar(50) NOT NULL DEFAULT 'cloud' COMMENT '模块来源:cloud 商城购买/local 本地自主研发';");

			$module_list = module_installed_list();
			$cloud_m_query_module_pageinfo = cloud_m_query(array(), 1);
			$cloud_m_query_module = $cloud_m_query_module_pageinfo['data'];
			if ($cloud_m_query_module_pageinfo['page'] > 1) {
				for($i = 2;$i <= $cloud_m_query_module['page']; $i++) {
					$cloud_m_query_module_i = cloud_m_query(array(), $i);
					$cloud_m_query_module = array_merge($cloud_m_query_module, $cloud_m_query_module_i['data']);
				}
			}

			if (!empty($cloud_m_query_module) && !empty($module_list)) {
				$cloud_m_query_module = array_keys($cloud_m_query_module);
				foreach ($module_list as $module_name => $module_info) {
					$where = array('name' => $module_name);
					$module_exist = pdo_get('modules', $where);
					if (!empty($module_exist)) {
						if (in_array($module_name, $cloud_m_query_module)) {
							pdo_update('modules', array('from' => 'cloud'), $where);
						} else {
							pdo_update('modules', array('from' => 'local'), $where);
						}
					}
				}
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		