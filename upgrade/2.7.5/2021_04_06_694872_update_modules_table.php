<?php

namespace We7\V275;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1617694872
 * @version 2.7.5
 */

class UpdateModulesTable {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('modules', 'status')) {
			pdo_query("ALTER TABLE " . tablename('modules') . " ADD `status` TINYINT(1) NOT NULL;");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
