<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1546943251
 * @version 1.8.8
 */

class AlterTableStatVisitIndex {

	/**
	 *  执行更新
	 */
	public function up() {
		if(pdo_tableexists('stat_ip_visit')) {
			$sql = "ALTER TABLE " . tablename('stat_ip_visit') . " RENAME TO " . tablename('stat_visit_ip');
			pdo_run($sql);
		}

		if(pdo_indexexists('stat_visit_ip', 'ip')){
			pdo_run("ALTER TABLE " .tablename('stat_visit_ip') ." DROP INDEX ip");
		}
		if(pdo_indexexists('stat_visit_ip', 'date')){
			pdo_run("ALTER TABLE " .tablename('stat_visit_ip') ." DROP INDEX date");
		}
		if(pdo_indexexists('stat_visit_ip', 'module')){
			pdo_run("ALTER TABLE " .tablename('stat_visit_ip') ." DROP INDEX module");
		}
		if(pdo_indexexists('stat_visit_ip', 'uniacid')){
			pdo_run("ALTER TABLE " .tablename('stat_visit_ip') ." DROP INDEX uniacid");
		}

		if(!pdo_indexexists('stat_visit_ip', 'ip_date_module_uniacid')){
			pdo_run("ALTER TABLE " .tablename('stat_visit_ip') ." ADD INDEX ip_date_module_uniacid (`ip`, `date`, `module`, `uniacid`)");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		