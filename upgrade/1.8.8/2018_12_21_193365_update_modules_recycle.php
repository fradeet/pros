<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1545305615
 * @version 1.8.8
 */

class UpdateModulesRecycle {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_fieldexists('modules_recycle', 'baiduapp_support')) {
			pdo_query("ALTER TABLE " . tablename('modules_recycle') . " ADD `baiduapp_support` tinyint(1) NOT NULL DEFAULT 0 COMMENT '默认0,为1时停用或删除百度小程序支持有效';");
		}
		if(!pdo_fieldexists('modules_recycle', 'toutiaoapp_support')) {
			pdo_query("ALTER TABLE " . tablename('modules_recycle') . " ADD `toutiaoapp_support` tinyint(1) NOT NULL DEFAULT 0 COMMENT '默认0,为1时停用或删除头条小程序支持有效';");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		