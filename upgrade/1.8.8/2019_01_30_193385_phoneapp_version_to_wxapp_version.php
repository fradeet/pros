<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1545305615
 * @version 1.8.8
 */

class PhoneappVersionToWxappVersion {

	/**
	 *  执行更新
	 */
	public function up() {
		$all_phone_version = pdo_getall('phoneapp_versions');
		if (!empty($all_phone_version)) {
			foreach ($all_phone_version as $version) {
				unset($version['id']);
				pdo_insert('wxapp_versions', $version);
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		