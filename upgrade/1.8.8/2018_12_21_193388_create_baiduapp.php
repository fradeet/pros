<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1545305615
 * @version 1.8.8
 */

class CreateBaiduapp {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_tableexists('account_baiduapp')){
			$sql = "CREATE TABLE `ims_account_baiduapp` (
					`acid` int(10) UNSIGNED NOT NULL,
					`uniacid` int(10) UNSIGNED NOT NULL,
					`name` varchar(30) NOT NULL,
					`appid` varchar(32) NOT NULL,
					`key` varchar(32) NOT NULL,
					`secret` varchar(32) NOT NULL,
					`description` varchar(255) NOT NULL,
					PRIMARY KEY (`acid`),
					KEY `uniacid` (`uniacid`)
					) DEFAULT CHARSET=utf8;";
			pdo_run($sql);
		}

	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		