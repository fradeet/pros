<?php

namespace We7\V2749;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1648533666
 * @version 2.7.49
 */

class InsertCannonFodder {
	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_tableexists('cannon_fodder')) {
			$sql = "CREATE TABLE `ims_cannon_fodder` (
				`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
				`domain` varchar(50) NOT NULL,
				PRIMARY KEY (`id`),
				INDEX `domain` (`domain`)
				) ENGINE=InnoDB COMMENT='炮灰域名表';";
			pdo_run($sql);
		}
		if (!pdo_fieldexists('users', 'bind_domain_id')) {
			pdo_query("ALTER TABLE " . tablename('users') . " ADD `bind_domain_id` INT UNSIGNED NOT NULL DEFAULT '0' COMMENT '炮灰域名表对应ID';");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {
	}
}
