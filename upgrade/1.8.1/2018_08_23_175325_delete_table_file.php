<?php

namespace We7\V181;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1535018005
 * @version 1.8.1
 */

class DeleteTableFile {

	/**
	 *  执行更新
	 */
	public function up() {
		$files = array(
			'articlecategory',
			'articlenews',
			'articlenotice',
			'coresendsmslog',
			'phoneappversions',
			'sitearticlecomment',
			'sitetemplates',
			'userspermission',
		);
		foreach ($files as $file) {
			$file = IA_ROOT . '/framework/table/' . $file . '.table.php';
			if (file_exists($file)) {
				unlink($file);
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}