<?php

namespace We7\V218;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1582285247
* @version 2.1.8
*/

class CreateShoppingFeedback {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_tableexists('shopping_feedback')) {
			pdo_run("CREATE TABLE `ims_shopping_feedback` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`weid` int(10) unsigned NOT NULL,
`openid` varchar(50) NOT NULL,
`type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1为维权，2为告擎',
`status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态0未解决，1用户同意，2用户拒绝',
`feedbackid` varchar(30) NOT NULL COMMENT '投诉单号',
`transid` varchar(30) NOT NULL COMMENT '订单号',
`reason` varchar(1000) NOT NULL COMMENT '理由',
`solution` varchar(1000) NOT NULL COMMENT '期待解决方案',
`remark` varchar(1000) NOT NULL COMMENT '备注',
`createtime` int(10) unsigned NOT NULL,
PRIMARY KEY (`id`),
KEY `idx_weid` (`weid`),
KEY `idx_feedbackid` (`feedbackid`),
KEY `idx_createtime` (`createtime`),
KEY `idx_transid` (`transid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
