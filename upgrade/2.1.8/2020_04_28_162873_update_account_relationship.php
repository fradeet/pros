<?php

	namespace We7\V218;

	defined('IN_IA') or exit('Access Denied');
	/**
	 * [WeEngine System] Copyright (c) 2014 W7.CC
	 * Time: 1588062877
	 * @version 2.1.8
	 */

	class UpdateAccountRelationship {

		/**
		 *  执行更新
		 */
		public function up() {
			$accound_uniacids = pdo_getall('account', array(), array('uniacid'));
			foreach ($accound_uniacids as $ask => $auv) {
				$owner_user = pdo_getcolumn('uni_account_users', array('uniacid' => $auv['uniacid'], 'role' => 'owner'), 'uid');
				if(empty($owner_user)) continue;
				$founder_user = pdo_getcolumn('users_founder_own_users', array('uid' => $owner_user), 'founder_uid');
				$accound_user_founder = pdo_get('uni_account_users', array('uniacid' => $auv['uniacid'], 'role' => 'vice_founder'), array('id', 'uid'));
				if(empty($founder_user)) {
					if (!empty($accound_user_founder)) pdo_delete('uni_account_users', array('id' => $accound_user_founder['id']));
				} else {
					if (!empty($accound_user_founder) && $accound_user_founder['uid'] != $founder_user) {
						pdo_update('uni_account_users', array('uid' => $founder_user), array('id' => $accound_user_founder['id']));
					} elseif (empty($accound_user_founder)) {
						pdo_insert('uni_account_users', array('uniacid' => $auv['uniacid'], 'uid' => $founder_user, 'role' => 'vice_founder', 'createtime' => 0));
					}
				}
			}
		}

		/**
		 *  回滚更新
		 */
		public function down() {


		}
	}
