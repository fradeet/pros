<?php

namespace We7\V204;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1556281440
 * @version 2.0.4
 */

class UpdateModuleFiles {

	/**
	 *  执行更新
	 */
	public function up() {
		load()->model('cache');
		load()->func('file');
		$addons_root_path = IA_ROOT. '/addons/';
		$modules_file = glob($addons_root_path . '*');

		function reduce_version($version) {
			$new_version_array = array();
			$version_info = explode('.', $version);
			$version_length = count($version_info);
			if (end($version_info) != 0) {
				for($i = 0;$i < $version_length; $i++) {
					if ($i == ($version_length - 1)) {
						$new_version_array[$i] = (intval($version_info[$i]) - 1);
					} else {
						$new_version_array[$i] = $version_info[$i];
					}
				}
			} else {
				$brought = false;
				for ($i = $version_length - 1;$i >= 0; $i--) {
					if ($brought) {
						$new_version_array[$i] = $version_info[$i];
						continue;
					}
					if ($version_info[$i] == 0) {
						$new_version_array[$i] = 9;
					}
					if ($version_info[$i] > 0) {
						$new_version_array[$i] = (intval($version_info[$i]) - 1);
						$brought = true;
					}
				}
				if (!$brought) {
					array_push($new_version_array, '0');
				}
				$new_version_array = array_reverse($new_version_array);
			}
			$new_version = implode('.', $new_version_array);
			return $new_version;
		}

		if (!empty($modules_file)) {
			foreach ($modules_file as $dir) {
				if (!is_dir($dir)) {
					continue;
				}
				$site_file = $dir . '/site.php';
				$module_file = $dir . '/module.php';
				$module_name = explode('/', $dir);
				$module_name = end($module_name);

				$search_file_string = '$GLOBALS[\'_' . chr('180') . chr('181') . chr('182') . '\']';

				$site_modify = $module_modify = false;
				$module_codefile = IA_ROOT . '/data/module/' . md5($_W['setting']['site']['key'] . $module_name . 'module.php') . '.php';
				$ignore_module_file = false;
				if (!file_exists($module_codefile) && file_exists($module_codefile . '111')) {
					@rename($module_codefile . '111', $module_codefile);
					@rename($dir . '/111site111.php', $site_file);
					@rename($dir . '/111module111.php', $module_file);
					$ignore_module_file = true;
				}
				$site_codefile = IA_ROOT . '/data/module/' . md5($_W['setting']['site']['key'] . $module_name . 'site.php') . '.php';
				if (file_exists($site_file)) {
					$site_file_string = file_get_contents($site_file);
					if (strpos($site_file_string, $search_file_string) && !file_exists($site_codefile)) {
						$site_modify = true;
					}
				}
				if (file_exists($module_file) && !$ignore_module_file) {
					$module_file_string = file_get_contents($module_file);
					if (strpos($module_file_string, $search_file_string) && !file_exists($module_codefile)) {
						$module_modify = true;
					}
				}
				if (!$site_modify && !$module_modify) {
					continue;
				}
				$module_info = pdo_get('modules', array('name' => $module_name));
				if (empty($module_info) || empty($module_info['version']) || !strpos($module_info['version'], '.')) {
					continue;
				}
				$new_version = reduce_version($module_info['version']);
				if ($site_modify) {
					file_put_contents($site_file, file_get_contents($site_file) . '  ');
				}
				if ($module_modify) {
					file_put_contents($module_file, file_get_contents($module_file) . '  ');
				}
				pdo_update('modules', array('version' => $new_version), array('mid' => $module_info['mid']));
				cache_updatecache();
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		