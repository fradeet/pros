<?php

namespace We7\V182;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1536321588
 * @version 1.8.2
 */

class AlterUniVerifycodeAddColumn {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('uni_verifycode', array('failed_count'))) {
			$table_name = tablename('uni_verifycode');
			$sql = "ALTER TABLE {$table_name} ADD `failed_count` int(10) DEFAULT 0 COMMENT '输入手机验证码错误次数';";
			pdo_run($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		