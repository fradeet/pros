<?php

namespace We7\V216;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1579507594
* @version 2.1.6
*/

class UpdateModulesTable {

/**
 *  执行更新
 */
public function up() {
	if (!pdo_fieldexists('modules', 'application_type')) {
		pdo_run("ALTER TABLE `ims_modules` ADD `application_type` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1：模块；2：模板' AFTER `name`;");
	}
	if (!pdo_fieldexists('modules', 'sections')) {
		pdo_run("ALTER TABLE `ims_modules` ADD `sections` INT(10) UNSIGNED NOT NULL COMMENT '模板导航位置设置' AFTER `cloud_record`;");
	}
	if (!pdo_fieldexists('modules_cloud', 'application_type')) {
		pdo_run("ALTER TABLE `ims_modules_cloud` ADD `application_type` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1：模块；2：模板' AFTER `name`;");
	}
}

/**
 *  回滚更新
 */
public function down() {


}
}
