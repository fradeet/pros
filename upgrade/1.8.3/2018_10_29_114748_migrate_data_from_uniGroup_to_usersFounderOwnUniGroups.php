<?php

namespace We7\V183;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1540784868
 * @version 1.8.3
 */

class MigrateDataFromUniGroupToUsersFounderOwnUniGroups {

	/**
	 *  执行更新
	 */
	public function up() {
		$founder_own_uni_groups = tablename('users_founder_own_uni_groups');
		$uni_group = tablename('uni_group');
		$sql = <<<EOF
INSERT INTO $founder_own_uni_groups(`founder_uid`, `uni_group_id`) select `owner_uid`, `id` from $uni_group where `owner_uid` != 0;
EOF;
		pdo_query($sql);
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		