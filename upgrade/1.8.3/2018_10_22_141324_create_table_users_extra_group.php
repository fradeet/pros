<?php

namespace We7\V183;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1540188804
 * @version 1.8.3
 */

class CreateTableUsersExtraGroup {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_tableexists('users_extra_group')) {
			$table_name = tablename('users_extra_group');
			$sql = <<<EOF
CREATE TABLE $table_name (
	`id` int(10) unsigned not null AUTO_INCREMENT,
	`uid` int(10) unsigned not null comment '用户id',
	`uni_group_id` int(10) unsigned not null default 0 comment '应用权限组id',
	`create_group_id` int(10) unsigned not null default 0 comment '帐号权限组id',
	PRIMARY KEY(`id`)
) DEFAULT CHARSET=utf8;
EOF;
			pdo_query($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		