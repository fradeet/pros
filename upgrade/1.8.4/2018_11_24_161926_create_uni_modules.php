<?php

namespace We7\V184;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1543047566
 * @version 1.8.4
 */

class CreateUniModules {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_tableexists('uni_modules')) {
			$sql = "CREATE TABLE IF NOT EXISTS `ims_uni_modules` (
					`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
					`uniacid` int(10) NOT NULL,
					`module_name` varchar(50) NOT NULL,
					PRIMARY KEY (`id`),
					KEY `module_name` (`module_name`),
					KEY `uniacid` (`uniacid`)
					) DEFAULT CHARSET=utf8;";
			pdo_run($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		