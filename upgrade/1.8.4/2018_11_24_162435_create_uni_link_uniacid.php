<?php

namespace We7\V184;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1543047875
 * @version 1.8.4
 */

class CreateUniLinkUniacid {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_tableexists('uni_link_uniacid')) {
			$sql = "CREATE TABLE IF NOT EXISTS `ims_uni_link_uniacid` (
					`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
					`uniacid` int(10) NOT NULL COMMENT '账号uniacid',
					`link_uniacid` int(10) NOT NULL COMMENT '关联账号',
					`version_id` int(10) NOT NULL COMMENT '版本ID',
					`module_name` varchar(255) NOT NULL COMMENT '模块标识'
					PRIMARY KEY (`id`),
					KEY `uniacid` (`uniacid`),
					KEY `link_uniacid` (`link_uniacid`),
					KEY `module_name` (`module_name`)
					) DEFAULT CHARSET=utf8;";
			pdo_run($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		