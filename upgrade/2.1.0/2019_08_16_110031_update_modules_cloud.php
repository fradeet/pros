<?php

namespace We7\V210;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1565924431
* @version 2.1.0
*/

class UpdateModulesCloud {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('modules_cloud', 'module_status')) {
			$sql = "ALTER TABLE " . tablename('modules_cloud') . " ADD COLUMN `module_status` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '模块状态 1 默认状态 2无废弃表数据或者废弃表已删除 3忽略'";
			pdo_run($sql);
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
