<?php

namespace We7\V210;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1565753527
* @version 2.1.0
*/

class UpdateUniSettings {

	/**
	 *  执行更新
	 */
	public function up() {
		if (pdo_fieldexists('uni_settings', 'uc')) {
			$sql = "ALTER TABLE " . tablename('uni_settings') . " DROP COLUMN `uc`";
			pdo_run($sql);
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
