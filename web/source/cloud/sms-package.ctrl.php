<?php
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC.
 */
defined('IN_IA') or exit('Access Denied');

load()->model('cloud');

$dos = array('smsTrade');
$do = in_array($do, $dos) ? $do : '';

if (empty($do)) {
	iajax(-1, '非法请求！');
}
if ('smsTrade' == $do) {
	$page = empty($_GPC['page']) ? 1 : intval($_GPC['page']);
	$status_order = empty($_GPC['status_order']) ? 0 : intval($_GPC['status_order']);
	$data = cloud_sms_trade($page, intval($_GPC['start_time']), intval($_GPC['end_time']), $status_order);

	if (isset($data['data'][0]['createtime']) && is_numeric($data['data'][0]['createtime'])) {
		foreach ($data['data'] as &$item) {
			$item['createtime'] = date('Y-m-d H:i:s', $item['createtime']);
		}
	}
	$data['page'] = $data['current_page'];
	$data['page_size'] = $data['per_page'];
	
	$sms_info = cloud_sms_info();
	if (is_error($sms_info)) {
		iajax(-1, $sms_info['message']);
	}
	$message = array(
		'sms_info' => $sms_info,
		'list' => $data
	);

	iajax(0, $message);
}